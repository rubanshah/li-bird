<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'LoginController@login');

Route::post('/reset-password', 'LoginController@resetPassword');

Route::middleware('auth:api')->get('/auth/me', function (Request $request) {
    return Auth::user();
});

Route::middleware('auth:api')->get('/logout', 'LoginController@logout');

Route::middleware('auth:api')->get('/roles', 'GeneralController@getRoles');

Route::middleware('auth:api')->get('/users', 'UserController@index')->middleware('can:lists,App\User');
Route::middleware('auth:api')->get('/users/{id}', 'UserController@find')->middleware('can:view,App\User,id');
Route::middleware('auth:api')->post('/users', 'UserController@create')->middleware('can:create,App\User,id');
Route::middleware('auth:api')->patch('users/{id}', 'UserController@update')->middleware('can:update,App\User');
Route::middleware('auth:api')->patch('users/{id}/change-password', 'UserController@changePassword')->middleware('can:changePassword,App\User,id');
Route::middleware('auth:api')->delete('users/{id}', 'UserController@delete')->middleware('can:delete,App\User');



Route::middleware('auth:api')->get('/vendors', 'VendorController@index')->middleware('can:listVendor,App\Contract');
Route::middleware('auth:api')->get('/vendors/{id}', 'VendorController@find')->middleware('can:viewVendor,App\Contract');
Route::middleware('auth:api')->post('/vendors','VendorController@create')->middleware('can:addVendor,App\Contract');
Route::middleware('auth:api')->patch('vendors/{id}', 'VendorController@update')->middleware('can:updateVendor,App\Contract');
Route::middleware('auth:api')->delete('vendors/{id}', 'VendorController@delete')->middleware('can:deleteVendor,App\Contract');


Route::middleware('auth:api')->get('/contracts','ContractController@index')->middleware('can:list,App\Contract');
Route::get('/contracts/types','ContractController@getContractTypes');

Route::middleware('auth:api')->get('/contracts/{id}','ContractController@find')->middleware('can:view,App\Contract');
Route::middleware('addAccessToken')->middleware('auth:api')->post('/contracts','ContractController@create')->middleware('can:create,App\Contract');
Route::middleware('auth:api')->patch('/contracts/{id}','ContractController@update')->middleware('can:update,App\Contract');
Route::middleware('auth:api')->delete('/contracts/{id}','ContractController@delete')->middleware('can:delete,App\Contract');
Route::middleware('auth:api')->get('/contracts/{id}/alerts','ContractController@getAlerts');
Route::middleware('auth:api')->post('/contracts/{id}/alerts','ContractController@saveAlerts');
Route::middleware('auth:api')->get('/contracts/{id}/logs','ContractController@getLogs')->middleware('can:view,App\Contract');
Route::middleware('auth:api')->get('/contracts/{cid}/logs/{rid}','ContractController@viewLog')->middleware('can:view,App\Contract');


Route::middleware('auth:api')->post('/upload-media','GeneralController@upload');
Route::get('/get-media/{filename}','GeneralController@getMedia');

Route::get('/notifications','NotificationController@contractExpiryNotification');


Route::middleware('auth:api')->get('/projects', 'ProjectController@index')->middleware('can:listProjects,App\Project');
Route::middleware('auth:api')->get('/projects/summary','ReportController@summary')->middleware('can:viewReport,App\Project');
Route::middleware('auth:api')->get('/projects/{id}', 'ProjectController@find');
Route::middleware('auth:api')->post('/projects','ProjectController@create')->middleware('can:addProject,App\Project');
Route::middleware('auth:api')->patch('projects/{id}', 'ProjectController@update')->middleware('can:updateProject,App\Project');
Route::middleware('auth:api')->delete('projects/{id}', 'ProjectController@delete')->middleware('can:deleteProject,App\Project');


Route::middleware('auth:api')->patch('/settings/saveContractEmail','SettingsController@saveContractEmail')->middleware('can:updateSettings,App\User');




