/*
SQLyog Ultimate v8.55 
MySQL - 5.7.18-log : Database - li-bird
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`li-bird` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `li-bird`;

/*Table structure for table `contract_alerts` */

DROP TABLE IF EXISTS `contract_alerts`;

CREATE TABLE `contract_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `unit` enum('d') COLLATE utf8mb4_unicode_ci DEFAULT 'd',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contract_alerts` */

insert  into `contract_alerts`(`id`,`contract_id`,`value`,`unit`,`created_at`,`updated_at`) values (17,2,6,'d','2018-03-29 13:20:31','2018-03-29 13:20:31');

/*Table structure for table `contract_docs` */

DROP TABLE IF EXISTS `contract_docs`;

CREATE TABLE `contract_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) DEFAULT NULL,
  `ref_num` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_contract_docs` (`contract_id`),
  CONSTRAINT `FK_contract_docs` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contract_docs` */

insert  into `contract_docs`(`id`,`contract_id`,`ref_num`,`media_id`,`created_by`,`updated_by`,`deleted_by`,`created_at`,`updated_at`,`deleted_at`) values (1,18,NULL,NULL,NULL,NULL,NULL,'2018-03-18 14:39:35','2018-03-18 14:39:35',NULL),(2,18,NULL,NULL,NULL,NULL,NULL,'2018-03-18 14:39:44','2018-03-18 14:39:44',NULL),(3,18,4,1,NULL,NULL,NULL,'2018-03-21 12:50:36','2018-03-21 12:50:36',NULL),(4,22,1,18,NULL,NULL,NULL,'2018-03-25 07:41:11','2018-03-25 07:41:11',NULL),(5,2,2,18,NULL,NULL,NULL,'2018-03-25 12:24:08','2018-03-25 12:24:08',NULL),(6,2,3,18,NULL,NULL,NULL,'2018-03-25 12:30:09','2018-03-25 12:30:09',NULL),(7,2,4,18,NULL,NULL,NULL,'2018-03-25 12:30:25','2018-03-25 12:30:25',NULL),(8,2,4,19,NULL,NULL,NULL,'2018-03-25 12:30:25','2018-03-25 12:30:25',NULL),(9,2,5,18,NULL,NULL,NULL,'2018-03-25 12:38:40','2018-03-25 12:38:40',NULL),(10,2,5,19,NULL,NULL,NULL,'2018-03-25 12:38:41','2018-03-25 12:38:41',NULL),(11,2,5,20,NULL,NULL,NULL,'2018-03-25 12:38:41','2018-03-25 12:38:41',NULL),(12,2,6,18,NULL,NULL,NULL,'2018-03-25 12:39:09','2018-03-25 12:39:09',NULL),(13,2,6,19,NULL,NULL,NULL,'2018-03-25 12:39:09','2018-03-25 12:39:09',NULL),(14,2,6,20,NULL,NULL,NULL,'2018-03-25 12:39:09','2018-03-25 12:39:09',NULL),(15,2,6,21,NULL,NULL,NULL,'2018-03-25 12:39:10','2018-03-25 12:39:10',NULL),(16,22,2,18,NULL,NULL,NULL,'2018-03-26 13:05:33','2018-03-26 13:05:33',NULL),(17,2,7,18,NULL,NULL,NULL,'2018-03-27 04:43:57','2018-03-27 04:43:57',NULL),(18,2,7,19,NULL,NULL,NULL,'2018-03-27 04:43:57','2018-03-27 04:43:57',NULL),(19,2,7,20,NULL,NULL,NULL,'2018-03-27 04:43:58','2018-03-27 04:43:58',NULL),(20,2,7,21,NULL,NULL,NULL,'2018-03-27 04:43:58','2018-03-27 04:43:58',NULL),(21,2,8,18,NULL,NULL,NULL,'2018-03-27 04:44:50','2018-03-27 04:44:50',NULL),(22,2,8,19,NULL,NULL,NULL,'2018-03-27 04:44:50','2018-03-27 04:44:50',NULL),(23,2,8,20,NULL,NULL,NULL,'2018-03-27 04:44:50','2018-03-27 04:44:50',NULL),(24,2,8,21,NULL,NULL,NULL,'2018-03-27 04:44:50','2018-03-27 04:44:50',NULL),(25,2,9,18,NULL,NULL,NULL,'2018-03-27 04:54:36','2018-03-27 04:54:36',NULL),(26,2,9,19,NULL,NULL,NULL,'2018-03-27 04:54:36','2018-03-27 04:54:36',NULL),(27,2,9,20,NULL,NULL,NULL,'2018-03-27 04:54:36','2018-03-27 04:54:36',NULL),(28,2,9,21,NULL,NULL,NULL,'2018-03-27 04:54:36','2018-03-27 04:54:36',NULL),(29,2,10,18,NULL,NULL,NULL,'2018-03-27 04:54:42','2018-03-27 04:54:42',NULL),(30,2,10,19,NULL,NULL,NULL,'2018-03-27 04:54:42','2018-03-27 04:54:42',NULL),(31,2,10,20,NULL,NULL,NULL,'2018-03-27 04:54:42','2018-03-27 04:54:42',NULL),(32,2,10,21,NULL,NULL,NULL,'2018-03-27 04:54:42','2018-03-27 04:54:42',NULL),(33,2,12,22,NULL,NULL,NULL,'2018-03-27 05:02:02','2018-03-27 05:02:02',NULL),(34,2,13,22,NULL,NULL,NULL,'2018-03-27 05:02:22','2018-03-27 05:02:22',NULL),(35,2,14,22,NULL,NULL,NULL,'2018-03-27 09:11:53','2018-03-27 09:11:53',NULL),(36,2,15,22,NULL,NULL,NULL,'2018-03-27 09:16:07','2018-03-27 09:16:07',NULL),(37,2,16,22,NULL,NULL,NULL,'2018-03-27 09:21:35','2018-03-27 09:21:35',NULL),(38,2,17,22,NULL,NULL,NULL,'2018-03-27 09:29:18','2018-03-27 09:29:18',NULL),(39,2,18,22,NULL,NULL,NULL,'2018-03-27 09:35:24','2018-03-27 09:35:24',NULL),(40,2,19,22,NULL,NULL,NULL,'2018-03-27 09:36:49','2018-03-27 09:36:49',NULL),(41,2,19,24,NULL,NULL,NULL,'2018-03-27 09:36:49','2018-03-27 09:36:49',NULL),(42,2,20,22,1,1,NULL,'2018-03-27 09:41:47','2018-03-27 09:41:47',NULL),(43,2,20,24,1,1,NULL,'2018-03-27 09:41:47','2018-03-27 09:41:47',NULL),(44,2,21,22,1,1,NULL,'2018-03-27 15:38:15','2018-03-27 15:38:15',NULL),(45,2,21,24,1,1,NULL,'2018-03-27 15:38:15','2018-03-27 15:38:15',NULL),(46,2,22,22,1,1,NULL,'2018-03-27 15:41:39','2018-03-27 15:41:39',NULL),(47,2,22,24,1,1,NULL,'2018-03-27 15:41:39','2018-03-27 15:41:39',NULL),(48,2,22,33,1,1,NULL,'2018-03-27 15:41:39','2018-03-27 15:41:39',NULL),(49,2,23,22,1,1,NULL,'2018-03-27 17:21:18','2018-03-27 17:21:18',NULL),(50,2,23,24,1,1,NULL,'2018-03-27 17:21:18','2018-03-27 17:21:18',NULL),(51,2,23,33,1,1,NULL,'2018-03-27 17:21:18','2018-03-27 17:21:18',NULL),(52,2,24,22,1,1,NULL,'2018-03-27 17:27:41','2018-03-27 17:27:41',NULL),(53,2,24,24,1,1,NULL,'2018-03-27 17:27:41','2018-03-27 17:27:41',NULL),(54,2,24,33,1,1,NULL,'2018-03-27 17:27:41','2018-03-27 17:27:41',NULL),(55,2,25,22,1,1,NULL,'2018-03-27 17:29:22','2018-03-27 17:29:22',NULL),(56,2,25,24,1,1,NULL,'2018-03-27 17:29:22','2018-03-27 17:29:22',NULL),(57,2,25,33,1,1,NULL,'2018-03-27 17:29:22','2018-03-27 17:29:22',NULL),(58,2,26,22,1,1,NULL,'2018-03-27 17:50:39','2018-03-27 17:50:39',NULL),(59,2,26,24,1,1,NULL,'2018-03-27 17:50:39','2018-03-27 17:50:39',NULL),(60,2,26,33,1,1,NULL,'2018-03-27 17:50:39','2018-03-27 17:50:39',NULL),(61,2,27,22,1,1,NULL,'2018-03-27 18:02:55','2018-03-27 18:02:55',NULL),(62,2,27,24,1,1,NULL,'2018-03-27 18:02:55','2018-03-27 18:02:55',NULL),(63,2,27,33,1,1,NULL,'2018-03-27 18:02:55','2018-03-27 18:02:55',NULL),(64,2,28,22,1,1,NULL,'2018-03-28 06:55:22','2018-03-28 06:55:22',NULL),(65,2,28,24,1,1,NULL,'2018-03-28 06:55:22','2018-03-28 06:55:22',NULL),(66,2,28,33,1,1,NULL,'2018-03-28 06:55:22','2018-03-28 06:55:22',NULL),(67,2,29,22,1,1,NULL,'2018-03-28 06:56:08','2018-03-28 06:56:08',NULL),(68,2,29,24,1,1,NULL,'2018-03-28 06:56:08','2018-03-28 06:56:08',NULL),(69,2,29,33,1,1,NULL,'2018-03-28 06:56:08','2018-03-28 06:56:08',NULL),(70,2,30,22,1,1,NULL,'2018-03-28 06:59:16','2018-03-28 06:59:16',NULL),(71,2,30,24,1,1,NULL,'2018-03-28 06:59:16','2018-03-28 06:59:16',NULL),(72,2,30,33,1,1,NULL,'2018-03-28 06:59:16','2018-03-28 06:59:16',NULL),(73,2,31,22,1,1,NULL,'2018-03-28 14:20:11','2018-03-28 14:20:11',NULL),(74,2,31,24,1,1,NULL,'2018-03-28 14:20:11','2018-03-28 14:20:11',NULL),(75,2,31,33,1,1,NULL,'2018-03-28 14:20:11','2018-03-28 14:20:11',NULL),(76,2,32,22,1,1,NULL,'2018-03-29 15:17:41','2018-03-29 15:17:41',NULL),(77,2,32,24,1,1,NULL,'2018-03-29 15:17:41','2018-03-29 15:17:41',NULL),(78,2,32,33,1,1,NULL,'2018-03-29 15:17:41','2018-03-29 15:17:41',NULL),(79,2,33,22,1,1,NULL,'2018-03-29 15:18:11','2018-03-29 15:18:11',NULL),(80,2,33,24,1,1,NULL,'2018-03-29 15:18:11','2018-03-29 15:18:11',NULL),(81,2,33,33,1,1,NULL,'2018-03-29 15:18:11','2018-03-29 15:18:11',NULL),(82,2,34,22,1,1,NULL,'2018-03-29 15:19:59','2018-03-29 15:19:59',NULL),(83,2,34,24,1,1,NULL,'2018-03-29 15:19:59','2018-03-29 15:19:59',NULL),(84,2,34,33,1,1,NULL,'2018-03-29 15:19:59','2018-03-29 15:19:59',NULL),(85,24,7,34,1,1,NULL,'2018-03-29 15:32:27','2018-03-29 15:32:27',NULL),(86,24,8,34,1,1,NULL,'2018-03-29 15:32:39','2018-03-29 15:32:39',NULL),(87,24,8,35,1,1,NULL,'2018-03-29 15:32:39','2018-03-29 15:32:39',NULL),(88,24,9,34,1,1,NULL,'2018-03-29 15:42:00','2018-03-29 15:42:00',NULL),(89,24,9,35,1,1,NULL,'2018-03-29 15:42:00','2018-03-29 15:42:00',NULL),(90,24,9,36,1,1,NULL,'2018-03-29 15:42:00','2018-03-29 15:42:00',NULL);

/*Table structure for table `contract_logs` */

DROP TABLE IF EXISTS `contract_logs`;

CREATE TABLE `contract_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `is_partial_payment` tinyint(1) DEFAULT NULL,
  `ref_num` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1-new,2-renew,0-close',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `FK_contract_logs` (`contract_id`),
  CONSTRAINT `FK_contract_logs` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contract_logs` */

insert  into `contract_logs`(`id`,`contract_id`,`title`,`start_date`,`end_date`,`vendor_id`,`amount`,`is_partial_payment`,`ref_num`,`status`,`created_by`,`updated_by`,`deleted_by`,`created_at`,`updated_at`,`deleted_at`,`summary`) values (1,2,'test','2015-09-09','2017-08-25',2,125.00,1,13,1,NULL,NULL,NULL,NULL,'2018-03-27 05:02:22',NULL,NULL),(2,2,'test','2015-09-09','2017-08-25',2,125.00,1,14,1,NULL,NULL,NULL,NULL,'2018-03-27 09:11:53',NULL,NULL),(3,2,'test','2015-09-09','2017-08-25',2,125.00,1,15,1,NULL,NULL,NULL,NULL,'2018-03-27 09:16:06',NULL,NULL),(4,2,'test','2015-09-09','2017-08-25',2,125.00,1,15,1,NULL,NULL,NULL,NULL,'2018-03-27 09:16:06',NULL,NULL),(5,2,'test','2015-09-09','2017-08-25',2,125.00,1,16,1,NULL,NULL,NULL,NULL,'2018-03-27 09:21:35',NULL,NULL),(6,2,'test','2015-09-09','2017-08-25',2,125.00,1,16,1,NULL,NULL,NULL,NULL,'2018-03-27 09:21:35',NULL,NULL),(7,2,'test','2015-09-09','2017-08-25',2,125.00,1,16,1,NULL,NULL,NULL,NULL,'2018-03-27 09:21:35',NULL,NULL),(8,2,'test','2015-09-09','2017-08-25',2,125.00,1,17,1,NULL,NULL,NULL,NULL,'2018-03-27 09:29:17',NULL,NULL),(9,2,'test','2015-09-09','2017-08-25',2,125.00,1,17,1,NULL,NULL,NULL,NULL,'2018-03-27 09:29:17',NULL,NULL),(10,2,'test','2015-09-09','2017-08-25',2,125.00,1,17,1,NULL,NULL,NULL,NULL,'2018-03-27 09:29:17',NULL,NULL),(11,2,'test','2015-09-09','2017-08-25',2,125.00,1,17,1,NULL,NULL,NULL,NULL,'2018-03-27 09:29:17',NULL,NULL),(12,2,'test','2015-09-09','2017-08-25',2,125.00,1,18,1,NULL,1,NULL,NULL,'2018-03-27 09:35:23',NULL,NULL),(13,2,'test','2015-09-09','2017-08-25',2,125.00,1,19,1,NULL,1,NULL,NULL,'2018-03-27 09:36:49',NULL,NULL),(14,12,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,NULL,NULL,NULL,'2018-03-18 13:56:49','2018-03-18 13:56:49',NULL,NULL),(15,2,'test','2015-09-09','2017-08-25',2,125.00,1,20,1,1,1,NULL,NULL,'2018-03-27 09:41:47',NULL,NULL),(16,2,'test','2015-09-09','2017-08-25',2,125.00,1,21,1,1,1,NULL,NULL,'2018-03-27 15:38:15',NULL,NULL),(17,2,'test','2015-09-09','2017-08-25',2,125.00,1,22,1,1,1,NULL,NULL,'2018-03-27 15:41:39',NULL,NULL),(18,2,'test','2015-09-09','2017-08-25',2,125.00,1,23,1,1,1,NULL,NULL,'2018-03-27 17:21:18',NULL,NULL),(19,2,'test','2015-09-09','2017-08-25',2,6010.00,1,24,1,1,1,NULL,NULL,'2018-03-27 17:27:41',NULL,NULL),(20,2,'test','2015-09-09','2017-08-25',2,60107.00,1,25,1,1,1,NULL,NULL,'2018-03-27 17:29:22',NULL,NULL),(21,2,'test','2015-09-09','2017-08-25',2,60200.00,1,26,1,1,1,NULL,NULL,'2018-03-27 17:50:39',NULL,NULL),(22,2,'test','2015-09-09','2017-08-25',2,60197.00,1,27,1,1,1,NULL,NULL,'2018-03-27 18:02:55',NULL,NULL),(23,2,'test','2015-09-09','2017-08-25',2,60197.00,1,28,1,1,1,NULL,NULL,'2018-03-28 06:55:22',NULL,NULL),(24,2,'test','2015-09-09','2017-08-25',2,60287.00,1,29,1,1,1,NULL,NULL,'2018-03-28 06:56:08',NULL,NULL),(25,19,'dummnuy','2015-09-09','2018-03-28',NULL,NULL,0,1,1,NULL,NULL,NULL,'2018-03-24 15:02:42','2018-03-24 15:02:42',NULL,NULL),(26,2,'test','2015-09-09','2017-08-25',2,60287.00,1,30,1,1,1,NULL,NULL,'2018-03-28 06:59:16',NULL,NULL),(27,2,'test','2015-09-09','2017-08-25',2,60197.00,1,31,1,1,1,NULL,NULL,'2018-03-28 14:20:11',NULL,NULL),(28,2,'test','2015-09-09','2017-08-25',2,60197.00,1,32,1,1,1,NULL,NULL,'2018-03-29 15:17:41',NULL,NULL),(29,2,'test','2015-09-09','2017-08-25',2,60197.00,1,33,1,1,1,NULL,NULL,'2018-03-29 15:18:11',NULL,NULL),(30,24,'just a test','2018-03-13','2018-03-29',3,800.00,0,1,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:23:19',NULL,NULL),(31,24,'just a test','2018-03-13','2018-03-29',3,800.00,0,2,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:23:39',NULL,NULL),(32,24,'just a test','2018-03-13','2018-03-29',3,800.00,0,3,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:24:39',NULL,NULL),(33,24,'just a test','2018-03-13','2018-03-29',3,8900.00,1,4,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:31:18',NULL,NULL),(34,24,'just a test','2018-03-13','2018-03-29',3,8900.00,1,5,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:31:30',NULL,NULL),(35,24,'just a test','2018-03-13','2018-03-29',3,18000.00,1,6,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:32:03',NULL,NULL),(36,24,'just a test','2018-03-13','2018-03-29',3,18000.00,1,7,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:32:26',NULL,NULL),(37,24,'just a test','2018-03-13','2018-03-29',3,18000.00,1,8,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:32:39',NULL,NULL);

/*Table structure for table `contracts` */

DROP TABLE IF EXISTS `contracts`;

CREATE TABLE `contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `is_partial_payment` tinyint(1) DEFAULT NULL,
  `ref_num` int(11) DEFAULT '1',
  `status` tinyint(4) DEFAULT '1' COMMENT '1-new,2-renew',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `FK_contracts_vendor` (`vendor_id`),
  CONSTRAINT `FK_contracts_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contracts` */

insert  into `contracts`(`id`,`title`,`start_date`,`end_date`,`vendor_id`,`amount`,`is_partial_payment`,`ref_num`,`status`,`created_by`,`updated_by`,`deleted_by`,`created_at`,`updated_at`,`deleted_at`,`summary`) values (2,'test','2015-09-09','2017-08-25',2,60197.00,1,34,1,1,1,NULL,NULL,'2018-03-29 15:19:59',NULL,NULL),(3,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:44:07','2018-03-18 13:44:07',NULL,NULL),(4,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:45:14','2018-03-18 13:45:14',NULL,NULL),(5,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:46:26','2018-03-18 13:46:26',NULL,NULL),(6,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:50:21','2018-03-18 13:50:21',NULL,NULL),(7,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:55:20','2018-03-18 13:55:20',NULL,NULL),(8,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:55:28','2018-03-18 13:55:28',NULL,NULL),(9,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:56:16','2018-03-18 13:56:16',NULL,NULL),(10,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:56:19','2018-03-18 13:56:19',NULL,NULL),(11,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:56:42','2018-03-27 05:39:15','2018-03-27 05:39:15',NULL),(12,'test','2015-09-13','2017-08-25',2,125.00,0,2,1,1,1,NULL,'2018-03-18 13:56:49','2018-03-27 11:17:02',NULL,NULL),(13,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:58:31','2018-03-18 13:58:31',NULL,NULL),(14,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:58:34','2018-03-18 13:58:34',NULL,NULL),(15,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 13:59:05','2018-03-18 13:59:05',NULL,NULL),(16,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 14:28:52','2018-03-18 14:28:52',NULL,NULL),(17,'test','2015-09-13','2017-08-25',2,125.00,NULL,1,1,1,1,NULL,'2018-03-18 14:29:19','2018-03-18 14:29:19',NULL,NULL),(18,'test','2015-09-13','2017-08-25',2,125.00,NULL,4,1,1,1,NULL,'2018-03-18 14:29:24','2018-03-21 12:50:35',NULL,NULL),(19,'dummnuy','2015-09-09','2018-03-28',2,1100.00,1,2,1,1,1,NULL,'2018-03-24 15:02:42','2018-03-28 12:31:55',NULL,NULL),(20,'nnn','2015-09-09','2018-03-28',2,NULL,0,1,1,1,1,NULL,'2018-03-24 15:04:16','2018-03-24 15:04:16',NULL,NULL),(21,'mmm','2015-09-09','2018-03-28',2,NULL,0,1,1,1,1,NULL,'2018-03-24 15:05:42','2018-03-24 15:05:42',NULL,NULL),(22,'test','2015-01-15','2015-01-31',3,300.00,1,2,1,1,1,NULL,'2018-03-25 07:41:09','2018-03-26 13:05:33',NULL,NULL),(23,'bbbbbb','2018-03-06','2018-03-22',3,500.00,0,1,1,1,1,NULL,'2018-03-28 12:57:46','2018-03-28 12:57:46',NULL,NULL),(24,'just a test','2018-03-13','2018-03-29',3,18000.00,1,9,1,1,1,NULL,'2018-03-29 15:23:19','2018-03-29 15:41:59',NULL,NULL);

/*Table structure for table `media` */

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `media` */

insert  into `media`(`id`,`collection_name`,`name`,`file_name`,`mime_type`,`created_at`,`updated_at`,`created_by`,`updated_by`) values (1,'contracts','cart-icon-redeem','cart-icon-redeem-1521897320.jpg','image/jpeg','2018-03-24 13:15:20','2018-03-24 13:15:20',NULL,NULL),(2,'contracts','cart-icon-redeem','cart-icon-redeem-1521898206.jpg','image/jpeg','2018-03-24 13:30:06','2018-03-24 13:30:06',NULL,NULL),(3,'contracts','keurig_rivo_capuccino_02','keurig_rivo_capuccino_02-1521902563.jpg','image/jpeg','2018-03-24 14:42:43','2018-03-24 14:42:43',NULL,NULL),(4,'contracts','cart-icon-redeem','cart-icon-redeem-1521958005.jpg','image/jpeg','2018-03-25 06:06:46','2018-03-25 06:06:46',NULL,NULL),(5,'contracts','cart-icon-redeem','cart-icon-redeem-1521958650.jpg','image/jpeg','2018-03-25 06:17:31','2018-03-25 06:17:31',NULL,NULL),(6,'contracts','cart-icon-redeem','cart-icon-redeem-1521958818.jpg','image/jpeg','2018-03-25 06:20:18','2018-03-25 06:20:18',NULL,NULL),(7,'contracts','keurig_rivo_capuccino_02','keurig_rivo_capuccino_02-1521958897.jpg','image/jpeg','2018-03-25 06:21:37','2018-03-25 06:21:37',NULL,NULL),(8,'contracts','cart-icon-redeem','cart-icon-redeem-1521959346.jpg','image/jpeg','2018-03-25 06:29:06','2018-03-25 06:29:06',NULL,NULL),(9,'contracts','cart-icon-redeem','cart-icon-redeem-1521959511.jpg','image/jpeg','2018-03-25 06:31:51','2018-03-25 06:31:51',NULL,NULL),(10,'contracts','cart-icon-redeem','cart-icon-redeem-1521960224.jpg','image/jpeg','2018-03-25 06:43:44','2018-03-25 06:43:44',NULL,NULL),(11,'contracts','cart-icon-redeem','cart-icon-redeem-1521960633.jpg','image/jpeg','2018-03-25 06:50:33','2018-03-25 06:50:33',NULL,NULL),(12,'contracts','cart-icon-redeem','cart-icon-redeem-1521960761.jpg','image/jpeg','2018-03-25 06:52:41','2018-03-25 06:52:41',NULL,NULL),(13,'contracts','cart-icon-redeem','cart-icon-redeem-1521960891.jpg','image/jpeg','2018-03-25 06:54:51','2018-03-25 06:54:51',NULL,NULL),(14,'contracts','cart-icon-redeem','cart-icon-redeem-1521960963.jpg','image/jpeg','2018-03-25 06:56:03','2018-03-25 06:56:03',NULL,NULL),(15,'contracts','apple_iphone_6_47_012','apple_iphone_6_47_012-1521961836.jpg','image/jpeg','2018-03-25 07:10:37','2018-03-25 07:10:37',NULL,NULL),(16,'contracts','cart-icon-redeem','cart-icon-redeem-1521978325.jpg','image/jpeg','2018-03-25 11:45:33','2018-03-25 11:45:33',NULL,NULL),(17,'contracts','cart-icon-redeem','cart-icon-redeem-1521980424.jpg','image/jpeg','2018-03-25 12:20:26','2018-03-25 12:20:26',NULL,NULL),(18,'contracts','apple_iphone_6_47_012','apple_iphone_6_47_012-1521980643.jpg','image/jpeg','2018-03-25 12:24:04','2018-03-25 12:24:04',NULL,NULL),(19,'contracts','apple_ipad-mini-3-retina_031','apple_ipad-mini-3-retina_031-1521981020.jpg','image/jpeg','2018-03-25 12:30:20','2018-03-25 12:30:20',NULL,NULL),(20,'contracts','li-bird-db','li-bird-db-1521981507.sql','text/plain','2018-03-25 12:38:30','2018-03-25 12:38:30',NULL,NULL),(21,'contracts','New Microsoft Office Word Document','New Microsoft Office Word Document-1521981546.docx','inode/x-empty','2018-03-25 12:39:06','2018-03-25 12:39:06',NULL,NULL),(22,'contracts','701_Roberts_Street_04-b','701_Roberts_Street_04-b-1522126919.jpg','image/jpeg','2018-03-27 05:02:00','2018-03-27 05:02:00',NULL,NULL),(23,'contracts','1800_South_Service_Rd_01-b','1800_South_Service_Rd_01-b-1522127491.jpg','image/jpeg','2018-03-27 05:11:31','2018-03-27 05:11:31',NULL,NULL),(24,'contracts','701_Roberts_Street_02-b','701_Roberts_Street_02-b-1522143405.jpg','image/jpeg','2018-03-27 09:36:46','2018-03-27 09:36:46',NULL,NULL),(25,'contracts','701_Roberts_Street_04-b','701_Roberts_Street_04-b-1522158591.jpg','image/jpeg','2018-03-27 13:49:52','2018-03-27 13:49:52',1,1),(26,'contracts','1800_South_Service_Rd_02-b','1800_South_Service_Rd_02-b-1522158698.jpg','image/jpeg','2018-03-27 13:51:38','2018-03-27 13:51:38',1,1),(27,'contracts','1800_South_Service_Rd_02-b','1800_South_Service_Rd_02-b-1522158844.jpg','image/jpeg','2018-03-27 13:54:04','2018-03-27 13:54:04',1,1),(28,'contracts','701_Roberts_Street_04-b','701_Roberts_Street_04-b-1522158857.jpg','image/jpeg','2018-03-27 13:54:17','2018-03-27 13:54:17',1,1),(29,'contracts','1800_South_Service_Rd_01-b','1800_South_Service_Rd_01-b-1522158861.jpg','image/jpeg','2018-03-27 13:54:21','2018-03-27 13:54:21',1,1),(30,'contracts','1800_South_Service_Rd_02-b','1800_South_Service_Rd_02-b-1522158865.jpg','image/jpeg','2018-03-27 13:54:25','2018-03-27 13:54:25',1,1),(31,'contracts','1800_South_Service_Rd_02-b','1800_South_Service_Rd_02-b-1522160804.jpg','image/jpeg','2018-03-27 14:26:45','2018-03-27 14:26:45',1,1),(32,'contracts','1800_South_Service_Rd_02-b','1800_South_Service_Rd_02-b-1522165218.jpg','image/jpeg','2018-03-27 15:40:18','2018-03-27 15:40:18',1,1),(33,'contracts','1800_South_Service_Rd_01-b','1800_South_Service_Rd_01-b-1522165293.jpg','image/jpeg','2018-03-27 15:41:33','2018-03-27 15:41:33',1,1),(34,'contracts','1800_South_Service_Rd_01-b','1800_South_Service_Rd_01-b-1522337544.jpg','image/jpeg','2018-03-29 15:32:25','2018-03-29 15:32:25',1,1),(35,'contracts','1800_South_Service_Rd_02-b','1800_South_Service_Rd_02-b-1522337556.jpg','image/jpeg','2018-03-29 15:32:36','2018-03-29 15:32:36',1,1),(36,'contracts','1800_South_Service_Rd_02-b','1800_South_Service_Rd_02-b-1522338116.jpg','image/jpeg','2018-03-29 15:41:57','2018-03-29 15:41:57',1,1);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',2),(4,'2016_06_01_000002_create_oauth_access_tokens_table',2),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',2),(6,'2016_06_01_000004_create_oauth_clients_table',2),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',2),(8,'2018_03_16_123746_create_media_table',3);

/*Table structure for table `oauth_access_tokens` */

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_access_tokens` */

insert  into `oauth_access_tokens`(`id`,`user_id`,`client_id`,`name`,`scopes`,`revoked`,`created_at`,`updated_at`,`expires_at`) values ('183abc53a730a39ff8ddb41a2f04d9243744b6f040f014a4632fcd377181031a97a9747afb965cc5',1,2,NULL,'[\"*\"]',0,'2018-03-13 09:58:30','2018-03-13 09:58:30','2019-03-13 09:58:30'),('3e1e296d1582f75431185f3ee622b50c70a8ec3c9f27d1076664bb7fc35a425e05187a7becf65c5a',1,2,NULL,'[\"*\"]',0,'2018-03-26 14:48:31','2018-03-26 14:48:31','2018-04-10 14:48:30'),('44cae35e9751613eb61d41f98c2bf32e721e0a3557dec90ebd1438813b516794c87092c7741cfe9a',1,2,NULL,'[\"*\"]',0,'2018-03-13 13:37:53','2018-03-13 13:37:53','2018-03-28 13:37:52'),('510d53265b4699beb5b66af4f8f7f3c345f31666aee2a897856972bb6bd46db3f6c01066847e8d13',1,2,NULL,'[\"*\"]',0,'2018-03-13 15:40:55','2018-03-13 15:40:55','2018-03-28 15:40:54'),('554ea98fd22eee1eddd6cfbfcf4b89e21566e0afe73bb0604d1678b730300c8af0730f0c54f9b654',1,2,NULL,'[\"*\"]',0,'2018-03-21 08:16:35','2018-03-21 08:16:35','2018-04-05 08:16:34'),('7e2754c504cbe76833ea45250f718845ad08e2cb9ea7840e4e7941498652ed28a76aced6490eb060',1,2,NULL,'[\"*\"]',0,'2018-03-15 17:05:37','2018-03-15 17:05:37','2018-03-30 17:05:36'),('aaf19b5a2055004056ceaa6696e0ab65325f3095587d6699c31a54fa165a2b362718d8872c52c536',1,2,NULL,'[\"*\"]',0,'2018-03-14 12:32:41','2018-03-14 12:32:41','2018-03-29 12:32:40'),('abddb78ff895f00025e255e507f53899102f8c70c8eb548e63c68aebb82a61768d7db5af64a5a73b',1,2,NULL,'[\"*\"]',0,'2018-03-13 15:51:30','2018-03-13 15:51:30','2018-03-28 15:51:30'),('b08a9ee718b43155984baea986744dbca1c1b5269ff2601e1350019f0dd9dd94aaa91b9604988ce7',1,2,NULL,'[\"*\"]',0,'2018-03-13 11:08:35','2018-03-13 11:08:35','2019-03-13 11:08:35'),('bc46f83f6639bc9b8584c2d0535fc64e121a64e5b40bd85706f156e84eb29ac81e636060cc0e173c',1,2,NULL,'[\"*\"]',0,'2018-03-14 08:10:24','2018-03-14 08:10:24','2018-03-29 08:10:23'),('df4c5c0d1b0f0df45fe8786f89fdbb8b13e65eaea823104b7bf1072985969f4f9a5929dcee3b12bd',1,2,NULL,'[\"*\"]',0,'2018-03-14 08:30:18','2018-03-14 08:30:18','2018-03-29 08:30:16');

/*Table structure for table `oauth_auth_codes` */

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_auth_codes` */

/*Table structure for table `oauth_clients` */

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_clients` */

insert  into `oauth_clients`(`id`,`user_id`,`name`,`secret`,`redirect`,`personal_access_client`,`password_client`,`revoked`,`created_at`,`updated_at`) values (1,NULL,'Li-Bird Personal Access Client','GUPXnfwrHKTB53kO47N11hoGyMBYVXdOobxH3i9v','http://localhost',1,0,0,'2018-03-13 09:21:06','2018-03-13 09:21:06'),(2,NULL,'Li-Bird Password Grant Client','ojADOm9y1P7sfHjMhnByRYuorPj8I0aibzoBJapq','http://localhost',0,1,0,'2018-03-13 09:21:07','2018-03-13 09:21:07');

/*Table structure for table `oauth_personal_access_clients` */

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_personal_access_clients` */

insert  into `oauth_personal_access_clients`(`id`,`client_id`,`created_at`,`updated_at`) values (1,1,'2018-03-13 09:21:07','2018-03-13 09:21:07');

/*Table structure for table `oauth_refresh_tokens` */

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_refresh_tokens` */

insert  into `oauth_refresh_tokens`(`id`,`access_token_id`,`revoked`,`expires_at`) values ('26e21b4a6ee7134d7ea57f3e9e7111b2cb8dee08731b913006489e63dabfb0f8977d0ce8d73c512c','3e1e296d1582f75431185f3ee622b50c70a8ec3c9f27d1076664bb7fc35a425e05187a7becf65c5a',0,'2018-04-25 14:48:30'),('4880440f943d22bb5bacfe0bfcc6e2fe3f4cb1ea57b4725b9895d5ab747a573d446234b524ae894b','510d53265b4699beb5b66af4f8f7f3c345f31666aee2a897856972bb6bd46db3f6c01066847e8d13',0,'2018-04-12 15:40:55'),('5d5c63f99abbbfdd684277c298cc1dbdea6e62b2d4a36bec28825286447d20ab612048211ad43c7e','bc46f83f6639bc9b8584c2d0535fc64e121a64e5b40bd85706f156e84eb29ac81e636060cc0e173c',0,'2018-04-13 08:10:24'),('5ff1dc734d73d9247ee3c8300b7f0ed425612e018d69ed93c42e4bf2b6fd7657acd307772edfca61','7e2754c504cbe76833ea45250f718845ad08e2cb9ea7840e4e7941498652ed28a76aced6490eb060',0,'2018-04-14 17:05:36'),('7760f58586436186dd4e383e03794a18e97a4bab97643c959b71b7dd730c648f978a40678fd1d550','44cae35e9751613eb61d41f98c2bf32e721e0a3557dec90ebd1438813b516794c87092c7741cfe9a',0,'2018-04-12 13:37:52'),('82024c1388cda2e0810b644f6e1a717f8fd64fcba71160b4cd78cc8b8ded05a252372b168ade313c','183abc53a730a39ff8ddb41a2f04d9243744b6f040f014a4632fcd377181031a97a9747afb965cc5',0,'2019-03-13 09:58:30'),('96b1ffa4c98ed1095c403df3f9b49f90744efd05a7fbdcf6c284fefc87db059abba464da61046daf','abddb78ff895f00025e255e507f53899102f8c70c8eb548e63c68aebb82a61768d7db5af64a5a73b',0,'2018-04-12 15:51:30'),('98b7c88032614a9385309f088e5aa7ac269c0c2047febf9bbbeb860d8e8f8c897a2947eafa80cd7b','554ea98fd22eee1eddd6cfbfcf4b89e21566e0afe73bb0604d1678b730300c8af0730f0c54f9b654',0,'2018-04-20 08:16:34'),('c484395b9d4813b765a2bfb8796ba5bca65cb49349f1ed26534e397ebec211122c8a09f1272676ce','aaf19b5a2055004056ceaa6696e0ab65325f3095587d6699c31a54fa165a2b362718d8872c52c536',0,'2018-04-13 12:32:40'),('c791f1b1fabe09ab7a43a0cd56afbc46c6cba538cb23c0566c41dfc447ca95554044fa56144317c1','b08a9ee718b43155984baea986744dbca1c1b5269ff2601e1350019f0dd9dd94aaa91b9604988ce7',0,'2019-03-13 11:08:36'),('d098892b0788a4439e368ee6db84fb72cab35de2106b416e0afcaee4d9bba84585b257a8eb31f262','df4c5c0d1b0f0df45fe8786f89fdbb8b13e65eaea823104b7bf1072985969f4f9a5929dcee3b12bd',0,'2018-04-13 08:30:16');

/*Table structure for table `partial_payments` */

DROP TABLE IF EXISTS `partial_payments`;

CREATE TABLE `partial_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `ref_num` int(11) NOT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_contract_partial_payments` (`contract_id`),
  CONSTRAINT `FK_contract_partial_payments` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `partial_payments` */

insert  into `partial_payments`(`id`,`contract_id`,`ref_num`,`amount`,`due_date`,`created_by`,`updated_by`,`deleted_by`,`created_at`,`updated_at`,`deleted_at`) values (1,2,1,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-18 12:28:24','0000-00-00 00:00:00',NULL),(2,5,1,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-18 13:46:26','2018-03-18 13:46:26','0000-00-00 00:00:00'),(3,16,1,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-18 14:28:52','2018-03-18 14:28:52','0000-00-00 00:00:00'),(4,17,1,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-18 14:29:19','2018-03-18 14:29:19','0000-00-00 00:00:00'),(5,18,1,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-18 14:29:24','2018-03-18 14:29:24','0000-00-00 00:00:00'),(6,18,2,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-18 14:39:35','2018-03-18 14:39:35','0000-00-00 00:00:00'),(7,18,3,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-18 14:39:44','2018-03-18 14:39:44','0000-00-00 00:00:00'),(8,18,4,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-21 12:50:36','2018-03-21 12:50:36','0000-00-00 00:00:00'),(9,22,1,1200.00,'2015-09-12',NULL,NULL,NULL,'2018-03-25 07:41:10','2018-03-25 07:41:10','0000-00-00 00:00:00'),(10,2,2,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-25 12:24:08','2018-03-25 12:24:08','0000-00-00 00:00:00'),(11,2,3,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-25 12:30:09','2018-03-25 12:30:09','0000-00-00 00:00:00'),(12,2,3,700.00,'2018-09-21',NULL,NULL,NULL,'2018-03-25 12:30:09','2018-03-25 12:30:09','0000-00-00 00:00:00'),(13,2,4,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-25 12:30:25','2018-03-25 12:30:25','0000-00-00 00:00:00'),(14,2,4,700.00,'2018-09-21',NULL,NULL,NULL,'2018-03-25 12:30:25','2018-03-25 12:30:25','0000-00-00 00:00:00'),(15,2,5,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-25 12:38:40','2018-03-25 12:38:40','0000-00-00 00:00:00'),(16,2,5,700.00,'2018-09-21',NULL,NULL,NULL,'2018-03-25 12:38:40','2018-03-25 12:38:40','0000-00-00 00:00:00'),(17,2,6,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-25 12:39:09','2018-03-25 12:39:09','0000-00-00 00:00:00'),(18,2,6,700.00,'2018-09-21',NULL,NULL,NULL,'2018-03-25 12:39:09','2018-03-25 12:39:09','0000-00-00 00:00:00'),(19,22,2,1200.00,'2015-09-12',NULL,NULL,NULL,'2018-03-26 13:05:33','2018-03-26 13:05:33','0000-00-00 00:00:00'),(20,2,7,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-27 04:43:57','2018-03-27 04:43:57','0000-00-00 00:00:00'),(21,2,7,700.00,'2018-09-21',NULL,NULL,NULL,'2018-03-27 04:43:57','2018-03-27 04:43:57','0000-00-00 00:00:00'),(22,2,8,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-27 04:44:50','2018-03-27 04:44:50','0000-00-00 00:00:00'),(23,2,8,700.00,'2018-09-21',NULL,NULL,NULL,'2018-03-27 04:44:50','2018-03-27 04:44:50','0000-00-00 00:00:00'),(24,2,9,54.00,'2017-08-25',NULL,NULL,NULL,'2018-03-27 04:54:36','2018-03-27 04:54:36','0000-00-00 00:00:00'),(25,2,9,700.00,'2018-09-21',NULL,NULL,NULL,'2018-03-27 04:54:36','2018-03-27 04:54:36','0000-00-00 00:00:00'),(26,2,13,600.00,'2017-09-08',NULL,NULL,NULL,'2018-03-27 05:02:22','2018-03-27 05:02:22','0000-00-00 00:00:00'),(27,2,14,600.00,'2017-09-08',NULL,NULL,NULL,'2018-03-27 09:11:53','2018-03-27 09:11:53','0000-00-00 00:00:00'),(28,2,15,600.00,'2017-09-08',NULL,NULL,NULL,'2018-03-27 09:16:07','2018-03-27 09:16:07','0000-00-00 00:00:00'),(29,2,16,600.00,'2017-09-08',NULL,NULL,NULL,'2018-03-27 09:21:35','2018-03-27 09:21:35','0000-00-00 00:00:00'),(30,2,17,600.00,'2017-09-08',NULL,NULL,NULL,'2018-03-27 09:29:17','2018-03-27 09:29:17','0000-00-00 00:00:00'),(31,2,18,600.00,'2017-09-08',NULL,NULL,NULL,'2018-03-27 09:35:24','2018-03-27 09:35:24','0000-00-00 00:00:00'),(32,2,19,600.00,'2017-09-08',NULL,NULL,NULL,'2018-03-27 09:36:49','2018-03-27 09:36:49','0000-00-00 00:00:00'),(33,2,20,600.00,'2017-09-08',1,1,NULL,'2018-03-27 09:41:47','2018-03-27 09:41:47','0000-00-00 00:00:00'),(34,2,21,600.00,'2017-09-08',1,1,NULL,'2018-03-27 15:38:15','2018-03-27 15:38:15','0000-00-00 00:00:00'),(35,2,22,600.00,'2017-09-08',1,1,NULL,'2018-03-27 15:41:39','2018-03-27 15:41:39','0000-00-00 00:00:00'),(36,2,23,600.00,'2017-09-08',1,1,NULL,'2018-03-27 17:21:18','2018-03-27 17:21:18','0000-00-00 00:00:00'),(37,2,24,6010.00,'2017-09-08',1,1,NULL,'2018-03-27 17:27:41','2018-03-27 17:27:41','0000-00-00 00:00:00'),(38,2,25,60107.00,'2017-09-08',1,1,NULL,'2018-03-27 17:29:22','2018-03-27 17:29:22','0000-00-00 00:00:00'),(39,2,26,60107.00,'2017-09-08',1,1,NULL,'2018-03-27 17:50:39','2018-03-27 17:50:39','0000-00-00 00:00:00'),(40,2,26,90.00,'2017-09-08',1,1,NULL,'2018-03-27 17:50:39','2018-03-27 17:50:39','0000-00-00 00:00:00'),(41,2,26,3.00,'2017-09-08',1,1,NULL,'2018-03-27 17:50:39','2018-03-27 17:50:39','0000-00-00 00:00:00'),(42,2,27,60107.00,'2017-09-08',1,1,NULL,'2018-03-27 18:02:55','2018-03-27 18:02:55','0000-00-00 00:00:00'),(43,2,27,90.00,'2017-09-08',1,1,NULL,'2018-03-27 18:02:55','2018-03-27 18:02:55','0000-00-00 00:00:00'),(44,2,28,60107.00,'2017-09-05',1,1,NULL,'2018-03-28 06:55:22','2018-03-28 06:55:22','0000-00-00 00:00:00'),(45,2,28,90.00,'2017-09-08',1,1,NULL,'2018-03-28 06:55:22','2018-03-28 06:55:22','0000-00-00 00:00:00'),(46,2,29,60107.00,'2017-09-05',1,1,NULL,'2018-03-28 06:56:08','2018-03-28 06:56:08','0000-00-00 00:00:00'),(47,2,29,90.00,'2017-09-08',1,1,NULL,'2018-03-28 06:56:08','2018-03-28 06:56:08','0000-00-00 00:00:00'),(48,2,29,90.00,'2018-03-01',1,1,NULL,'2018-03-28 06:56:08','2018-03-28 06:56:08','0000-00-00 00:00:00'),(49,2,30,60107.00,'2017-09-19',1,1,NULL,'2018-03-28 06:59:16','2018-03-28 06:59:16','0000-00-00 00:00:00'),(50,2,30,90.00,'2017-09-08',1,1,NULL,'2018-03-28 06:59:16','2018-03-28 06:59:16','0000-00-00 00:00:00'),(51,2,30,90.00,'2018-03-01',1,1,NULL,'2018-03-28 06:59:16','2018-03-28 06:59:16','0000-00-00 00:00:00'),(52,19,2,200.00,'2018-03-28',1,1,NULL,'2018-03-28 12:31:55','2018-03-28 12:31:55','0000-00-00 00:00:00'),(53,19,2,900.00,'2018-03-29',1,1,NULL,'2018-03-28 12:31:55','2018-03-28 12:31:55','0000-00-00 00:00:00'),(54,2,31,60107.00,'2017-09-19',1,1,NULL,'2018-03-28 14:20:11','2018-03-28 14:20:11','0000-00-00 00:00:00'),(55,2,31,90.00,'2017-09-08',1,1,NULL,'2018-03-28 14:20:11','2018-03-28 14:20:11','0000-00-00 00:00:00'),(56,2,32,60107.00,'2017-09-19',1,1,NULL,'2018-03-29 15:17:41','2018-03-29 15:17:41','0000-00-00 00:00:00'),(57,2,32,90.00,'2017-09-08',1,1,NULL,'2018-03-29 15:17:41','2018-03-29 15:17:41','0000-00-00 00:00:00'),(58,2,33,60107.00,'2017-09-19',1,1,NULL,'2018-03-29 15:18:11','2018-03-29 15:18:11','0000-00-00 00:00:00'),(59,2,33,90.00,'2017-09-08',1,1,NULL,'2018-03-29 15:18:11','2018-03-29 15:18:11','0000-00-00 00:00:00'),(60,2,34,60107.00,'2017-09-19',1,1,NULL,'2018-03-29 15:19:59','2018-03-29 15:19:59','0000-00-00 00:00:00'),(61,2,34,90.00,'2017-09-08',1,1,NULL,'2018-03-29 15:19:59','2018-03-29 15:19:59','0000-00-00 00:00:00'),(62,24,4,900.00,'2018-03-27',1,1,NULL,'2018-03-29 15:31:18','2018-03-29 15:31:18','0000-00-00 00:00:00'),(63,24,4,8000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:31:18','2018-03-29 15:31:18','0000-00-00 00:00:00'),(64,24,5,900.00,'2018-03-27',1,1,NULL,'2018-03-29 15:31:30','2018-03-29 15:31:30','0000-00-00 00:00:00'),(65,24,5,8000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:31:30','2018-03-29 15:31:30','0000-00-00 00:00:00'),(66,24,6,9000.00,'2018-03-27',1,1,NULL,'2018-03-29 15:32:03','2018-03-29 15:32:03','0000-00-00 00:00:00'),(67,24,6,8000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:32:03','2018-03-29 15:32:03','0000-00-00 00:00:00'),(68,24,6,1000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:32:03','2018-03-29 15:32:03','0000-00-00 00:00:00'),(69,24,7,9000.00,'2018-03-27',1,1,NULL,'2018-03-29 15:32:26','2018-03-29 15:32:26','0000-00-00 00:00:00'),(70,24,7,8000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:32:26','2018-03-29 15:32:26','0000-00-00 00:00:00'),(71,24,7,1000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:32:26','2018-03-29 15:32:26','0000-00-00 00:00:00'),(72,24,8,9000.00,'2018-03-27',1,1,NULL,'2018-03-29 15:32:39','2018-03-29 15:32:39','0000-00-00 00:00:00'),(73,24,8,8000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:32:39','2018-03-29 15:32:39','0000-00-00 00:00:00'),(74,24,8,1000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:32:39','2018-03-29 15:32:39','0000-00-00 00:00:00'),(75,24,9,9000.00,'2018-03-27',1,1,NULL,'2018-03-29 15:41:59','2018-03-29 15:41:59','0000-00-00 00:00:00'),(76,24,9,8000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:42:00','2018-03-29 15:42:00','0000-00-00 00:00:00'),(77,24,9,1000.00,'2018-03-21',1,1,NULL,'2018-03-29 15:42:00','2018-03-29 15:42:00','0000-00-00 00:00:00');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `perm_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`perm_name`,`desc`) values (1,'view-vendor',NULL),(2,'add-vendor',NULL),(3,'update-vendor',NULL),(4,'view-contract',NULL),(5,'add-contract',NULL),(6,'update-contract',NULL);

/*Table structure for table `role_perm` */

DROP TABLE IF EXISTS `role_perm`;

CREATE TABLE `role_perm` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned DEFAULT NULL,
  `perm_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_role_perm_roles_role_id` (`role_id`),
  KEY `FK_role_perm_permissions_perm_id` (`perm_id`),
  CONSTRAINT `FK_role_perm_permissions_perm_id` FOREIGN KEY (`perm_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `FK_role_perm_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_perm` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles-role-unique` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role`,`name`,`description`) values (1,'administrator','Administrator','administrator full control'),(5,'contract-manager','Contract Manager','Full Control of Contract Management');

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_roles_users_user_id` (`user_id`),
  KEY `FK_user_roles_roles_role_id` (`role_id`),
  CONSTRAINT `FK_user_roles_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FK_user_roles_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`user_id`,`role_id`) values (3,1,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`address`,`phone`,`password`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values (1,'admin','admin@example.com','thimi, Bhaktapur','654368','$2y$10$X9RXDe2EPMkDvSMHW0bYEedV4/kF4qj4R3FaP98JFBIZXv2eFFoaG',NULL,'2018-03-13 09:38:17','2018-03-28 07:08:20',NULL),(5,'dsfds','dsf@dfsd.com','ktm',NULL,'$2y$10$vO8m5hxyiMgoeu4O7ghWDeuagpcMccGf.tYUh7avOigftRm8aRAKi',NULL,'2018-03-15 16:38:31','2018-03-15 16:38:31',NULL),(6,'dsfds','dsf@dfsd.com3','ktm',NULL,'$2y$10$Hyyj3wMzQikDrP3qbMVTy.NLkDlyP.uPAhKBd1f2OIxNxaXbDtdCW',NULL,'2018-03-15 16:43:38','2018-03-15 16:43:38',NULL),(7,'dsfds','dsf@dfsd.com9','ktm',NULL,'$2y$10$ft399ibSJnCSc1OtZS1YcuYGrrXiJ8QUxc41CR88NymevnpUPXYrq',NULL,'2018-03-15 16:50:09','2018-03-15 16:50:09',NULL),(8,'dsfds','dsf@dfsd.com0','ktm',NULL,'$2y$10$/88l31V96d/1AFHooSYIouiaMxyWDQzQM2o5slR4h94pmte3kZ30i',NULL,'2018-03-15 16:51:04','2018-03-15 16:51:04',NULL),(9,'ruban','rubanshah9@gmail.com','thimi, Bhaktapur',NULL,'$2y$10$HFfsdhImueCqFrULVy54me4tezHgdGs7X60wyEAjq3ka8WwhEUzxe',NULL,'2018-03-16 07:54:58','2018-03-16 08:20:05',NULL),(10,'fd','rubanshah@gmail.com','ktme',NULL,'$2y$10$l5tKEe4TpMGYN91ifaDlPeHneRvU0mk5AJ.5oP7KC6M78mA63fOIW',NULL,'2018-03-16 08:20:19','2018-03-26 14:17:33',NULL);

/*Table structure for table `vendors` */

DROP TABLE IF EXISTS `vendors`;

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vendors` */

insert  into `vendors`(`id`,`name`,`address`,`phone`,`email`,`created_at`,`updated_at`,`deleted_at`) values (2,'vvvvvvvvvvv','thime','123432','rubanshah@gmail.com','2018-03-15 12:58:20','2018-03-15 12:58:36',NULL),(3,'test vendor','ktm','2432','exaamkdf@example.com','2018-03-19 08:10:38','2018-03-19 08:10:38',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
