<?php
namespace App\Repositories;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Contract;
use App\ContractLog;
use Illuminate\Support\Collection;
use Notification;
use App\Notifications\ContractCreation;


class ContractRepository {
    use ValidatesRequests;

    public function getAll($data)
    {
        $q = Contract::with('vendor','contractType', 'project');

        if(isset($data['title']) && $data['title']){
            $q->where('title','like','%'.$data['title'].'%');
        }
        if(isset($data['vendor_id']) && $data['vendor_id']){
            $q->where('vendor_id','=',$data['vendor_id']);
        }
        
        if((isset($data['date_from']) && $data['date_from']) && (isset($data['date_from']) && $data['date_from'])){
            $q->whereBetween('start_date',[$data['date_from'],$data['date_to']]);
        }
        

        if(isset($data['project_id']) && $data['project_id']){
            $q->where('project_id','=',$data['project_id']);
        }

        if(isset($data['contract_type_id']) && $data['contract_type_id']){
            $q->where('contract_type_id','=',$data['contract_type_id']);
        }

        if( (isset($data['amount_from']) && $data['amount_from']) && (isset($data['amount_to']) && $data['amount_to']) ){
            $q->whereBetween('amount',[$data['amount_from'], $data['amount_to']]);
        }else if(isset($data['amount_from']) && $data['amount_from']){
            $q->where('amount','>=',$data['amount_from']);
        }else if(isset($data['amount_to']) && $data['amount_to']){
            $q->where('amount','<=',$data['amount_to']);
        }


        if(isset($data['status']) && $data['status']){
            $q->where('status','=',$data['status']);
        }

        if(isset($data['id']) && $data['id']){
            $q->where('id','=',$data['id']);
        }


        if(isset($data['export'])){
            $result = $q->orderBy('id','desc')->get();
            $collection = new Collection();

            $collection->push(['Contract','Project','Vendor','Type','Amount','Start Date', 'End Date','Contract Id','Status']);

            foreach($result as $row){
                $d=[];
                $d['title'] = $row->title;
                $d['project'] = $row->project->name;
                $d['vendor'] = $row->vendor->name;
                $d['type'] = $row->contractType->name;
                $d['amount'] = $row->amount;
                $d['start_date'] = $row->start_date;
                $d['end_date'] = $row->end_date;
                $d['ref_num'] = $row->id;

                $d['status'] = null;
                if($row->status == 1){
                    $d['status'] = 'On Going';
                }else if ($row->status == 2){
                    $d['status'] = 'Complete';
                }
                $collection->push($d);
            }
            
            $filename = 'contracts.xlsx';
            return $collection->downloadExcel($filename);
        }

        return $q->orderBy('id','desc')->paginate(10);
    }

    public function find($id){

        $contract = Contract::find($id);
        //return $contract;
        return Contract::with(['partialPayment'=>function($q) use ($contract){
            $q->where('ref_num','=',$contract->ref_num);
        },
        'docs'=>function($q) use ($contract){
            $q->where('ref_num','=',$contract->ref_num);
            $q->with('media');
        },
        'reportingDate'=>function($q) use ($contract){
            $q->where('ref_num', '=', $contract->ref_num);
        },
        'vendor','creator','editor','alerts','contractType','project'])->find($id);
    }

    public  function create($data){

        $validator = Validator::make($data,['title' => 'required',
            'vendor_id' => 'required',
            'contract_type_id' => 'required',
            'project_id' => 'required',
            'status' => 'required',
            'amount' => 'required|numeric',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d|after:start_date',
            'is_partial_payment' => 'required',
            'partial_payment.*.amount' => 'required|numeric',
            'partial_payment.*.due_date' => 'required|date_format:Y-m-d',
            'reporting_date.*.date'=> 'required|date_format:Y-m-d'
            ])->validate();


        $contract = Contract::create($data);
        $contract = Contract::find($contract->id);

        $this->insertRelation($data, $contract);

            ///send email notification
                    //fetch email from project leads
            foreach ($contract->project->leads as $row) {
                    $notification  = notification::route('mail',$row->email);
                    $notification->notify(new ContractCreation($contract));
            }
            /////////////



        return $this->find($contract->id);
    }

    /**
    param data request
    $contract Contract
    */
    public function insertRelation($data,Contract $contract){
          if(isset($data['partial_payment']) && $data['is_partial_payment']){
            data_fill($data,'partial_payment.*.ref_num', $contract->ref_num); //add field ref_num if missing
            data_set($data,'partial_payment.*.ref_num', $contract->ref_num); //set field ref_num value
            $contract->partialPayment()->createMany($data['partial_payment']);
        }

        if(isset($data['docs'])){
            data_fill($data,'docs.*.ref_num', $contract->ref_num); //add field ref_num if missing
            data_set($data,'docs.*.ref_num', $contract->ref_num); //set field ref_num value
            $contract->docs()->createMany($data['docs']);
        }

       if(isset($data['reporting_date']) && $data['reporting_date']){
            data_fill($data,'reporting_date.*.ref_num', $contract->ref_num); //add field ref_num if missing
            data_set($data,'reporting_date.*.ref_num', $contract->ref_num); //set field ref_num value
            $contract->reportingDate()->createMany($data['reporting_date']);
        }

    }
    

    public function update($id,$data){

    $validator = Validator::make($data,['title' => 'required',
            'vendor_id' => 'required',
            'contract_type_id' => 'required',            
            'project_id' => 'required',
            'status' => 'required',
            'amount' => 'required|numeric',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d|after:start_date',
            'is_partial_payment' => 'required',
            'partial_payment.*.amount' => 'required|numeric',
            'partial_payment.*.due_date' => 'required|date_format:Y-m-d',
            'reporting_date.*.date'=> 'required|date_format:Y-m-d'
            ])->validate();

        $contract = Contract::find($id);

        //add to contracct log
        $contractLog = new ContractLog($contract->toArray());
        $contractLog->contract_id = $contract->id;
        $contractLog->save();
       
        //increase ref_num
        $contract->fill($data);
        $contract->ref_num = $contract->ref_num + 1 ;
        $contract->save();

        $this->insertRelation($data,$contract);

        return $this->find($contract->id);
      
    }

    public function delete($id){
        return Contract::destroy($id);
    }

    public function getLogs($id){
        return contractLog::with('editor')->where('contract_id','=',$id)->orderBy('ref_num','desc')->get();
    }

    public function viewLog($cid,$review_id){
        $contract = contractLog::where('contract_id','=',$cid)->where('ref_num','=',$review_id)->first();
        return ContractLog::with(['partialPayment'=>function($q) use ($contract){
            $q->where('ref_num','=',$contract->ref_num);
        },
        'docs'=>function($q) use ($contract){
            $q->where('ref_num','=',$contract->ref_num);
            $q->with('media');
        },'vendor','editor'])->where('contract_id','=',$cid)->where('ref_num','=',$review_id)->first();
    }

}