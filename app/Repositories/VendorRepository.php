<?php
namespace App\Repositories;

use App\Vendor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;

class VendorRepository {
    use ValidatesRequests;

    public function getAll()
    {
        return Vendor::all() ;
    }

    public function find($id){
        return Vendor::find($id);
    }

    public  function create($data){
        $validation = Validator::make($data,['name' => 'required',
            'email'=> "required|email|unique:vendors",
            'address'=> 'required',
            'phone'=> 'required',
        ])->validate();

        $vendor = new Vendor();
        $vendor->fill($data);
       
        $vendor->save();

        return $vendor;
    }

    public function update($id,$data){
        $validation = Validator::make($data,['name' => 'required',
            'email'=> "required|email|unique:vendors,id,$id",
            'address'=> 'required',
        ])->validate();

        $vendor = vendor::find($id);
        $vendor->fill($data);
        $vendor->save();

        return $vendor;
    }

    public function delete($id){
        return vendor::destroy($id);
    }

}