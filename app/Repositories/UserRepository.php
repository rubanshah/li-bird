<?php
namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Notifications\UserRegistered;

class UserRepository {
    use ValidatesRequests;

    public function getAll()
    {
        return User::with('roles')->paginate(1000);
    }

    public function find($id){
        return User::with('roles')->find($id);
    }


    public  function create($data){
        $validation = Validator::make($data,['name' => 'required',
            'phone' => 'required',
            'email'=> "required|email|unique:users",
            'address'=> 'required',
        ])->validate();

        $user = new User();
        $user->fill($data);

        $password = str_random(8);//generate random password
        $user->password = Hash::make($password);

        $user->save();

        //send notification
        $user->notify(new UserRegistered($user, $password));

        if(isset($data['roles'][0]) && isset($data['roles'][0]['id'])){
            $user->roles()->sync($data['roles'][0]['id']);
        }

        return $user;
    }

    public function update($id,$data){
        $validation = Validator::make($data,['name' => 'required',
            'phone' => 'required',
            'email'=> "required|email|unique:users,id,$id",
            'address'=> 'required',
        ])->validate();

        $user = User::find($id);
        $user->fill($data);
        $user->save();

        if(isset($data['roles'][0]) && isset($data['roles'][0]['id'])){
            $user->roles()->sync($data['roles'][0]['id']);
        }
        return $user;
    }

    public function delete($id){
        return User::destroy($id);
    }

    public function changePassword($id, $data){
         $validation = Validator::make($data,['password' => 'required|min:6',
            'password_confirm' => 'required|same:password',
        ])->validate();

        $user = User::find($id);
        $user->password = bcrypt($data['password']);
        $user->save();
        return $user;
    }
}