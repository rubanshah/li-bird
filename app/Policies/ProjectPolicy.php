<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

use Helper;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function listProjects(User $user){
        if (auth()->user()->tokenCan('list-project')){
            return true;
        }
        return false;
    }

    public function addProject(User $user){
        if (auth()->user()->tokenCan('add-project')){
            return true;
        }
        return false;
    }

    public function updateProject(User $user){
        if (auth()->user()->tokenCan('update-project')){
            return true;
        }
        return false;
    }

    public function deleteProject(User $user){
        if (auth()->user()->tokenCan('delete-project')){
            return true;
        }
        return false;
    }

    public function viewReport(){
     if (auth()->user()->tokenCan('view-report')){
            return true;
        }
        return false;   
    }

    public function before(){
        if(Helper::is_admin()){
            return true;
        }
    }
}
