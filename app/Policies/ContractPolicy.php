<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;


use Helper;

class ContractPolicy
{
    use HandlesAuthorization;

    public function list(User $user)
    {
        if (auth()->user()->tokenCan('view-contract')){
            return true;
        }
        return false;
    }


    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        if (auth()->user()->tokenCan('view-contract')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (auth()->user()->tokenCan('add-contract')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        if (auth()->user()->tokenCan('update-contract')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        if (auth()->user()->tokenCan('delete-contract')){
            return true;
        }
        return false;
    }


    public function before(){
        if(Helper::is_admin()){
            return true;
        }
    }

    public function listVendor(){
        if (auth()->user()->tokenCan('view-vendor')){
            return true;
        }
        return false;
    }

    public function viewVendor(){
        if (auth()->user()->tokenCan('view-vendor')){
            return true;
        }
        return false;
    }


    public function addVendor(){
        if (auth()->user()->tokenCan('add-vendor')){
            return true;
        }
        return false;
    }

    public function updateVendor(){
        if (auth()->user()->tokenCan('update-vendor')){
            return true;
        }
        return false;
    }

    public function deleteVendor(){
        if (auth()->user()->tokenCan('delete-vendor')){
            return true;
        }
        return false;
    }

}
