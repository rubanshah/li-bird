<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Media extends Model
{
    use Userstamps;

    protected function getUserClass()
    {
        if (get_class(auth()) === 'Illuminate\Auth\Guard') {
            return auth() -> getProvider() -> getModel();
        }

        return auth() -> guard('web') -> getProvider() -> getModel();
    }


    protected $table = 'media';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['collection_name', 'name', 'file_name', 'mime_type'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

}
