<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ContractAlert extends Model
{


    protected $table = 'contract_alerts';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract_id', 'value'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

}

