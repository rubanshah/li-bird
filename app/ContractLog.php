<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;


class ContractLog extends Model
{
	use SoftDeletes, Userstamps;

    protected function getUserClass()
    {
        if (get_class(auth()) === 'Illuminate\Auth\Guard') {
            return auth() -> getProvider() -> getModel();
        }

        return auth() -> guard('web') -> getProvider() -> getModel();
    }


     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'start_date', 'end_date', 'vendor_id', 'amount', 'is_partial_payment','ref_num','status','created_by','updated_by','deleted_by',
        'created_at','updated_at','deleted_at','summary'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function partialPayment(){
        return $this->hasMany('App\PartialPayment','contract_id','contract_id');
    }

    public function docs(){
        return $this->hasMany('App\ContractDocs','contract_id','contract_id');
    }

    public function vendor(){
        return $this->hasOne('App\Vendor','id','vendor_id');
    }

}
