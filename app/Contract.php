<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Contract extends Model
{
	use SoftDeletes, Userstamps;

    protected function getUserClass()
    {
        if (get_class(auth()) === 'Illuminate\Auth\Guard') {
            return auth() -> getProvider() -> getModel();
        }

        return auth() -> guard('web') -> getProvider() -> getModel();
    }

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'start_date', 'end_date', 'vendor_id', 'amount', 'is_partial_payment','contract_type_id', 'status', 'project_id', 'remark' ,'bond_type'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function partialPayment(){
        return $this->hasMany('App\PartialPayment');
    }

    public function docs(){
        return $this->hasMany('App\ContractDocs');
    }

    public function vendor(){
        return $this->hasOne('App\Vendor','id','vendor_id')->withTrashed();
    }

    public function alerts(){
        return $this->hasMany('App\ContractAlert');   
    }

    public function contractType(){
        return $this->hasOne('App\ContractType','id','contract_type_id');      
    }

    public function reportingDate(){
        return $this->hasMany('App\ReportingDate');      
    }

    public function project(){
        return $this->hasOne('App\Project','id','project_id')->withTrashed();   
    }

}
