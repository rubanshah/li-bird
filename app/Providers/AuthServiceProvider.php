<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use Carbon\Carbon;
use App\Policies\UserPolicy;
use App\Policies\ContractPolicy;
use App\Policies\ProjectPolicy;

use App\User;
use App\Contract;
use App\Project;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Contract::class => ContractPolicy::class,
        Project::class => ProjectPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        Passport::tokensCan([
            'view-vendor' => 'View Vendor',
            'add-vendor' => 'Add Vendor',
            'update-vendor' => 'Update Vendor',
            'delete-vendor' => 'Delete Vendor',
            
            'view-contract' => 'View Contract',
            'add-contract' => 'Add Contract',
            'update-contract' => 'Update Contract',
            'delete-contract' => 'Delete Contract',


            'view-project' => 'view list projects',
            'add-project' => 'Add Project',
            'update-project' => 'Update Project',
            'delete-project' => 'Delete Project',
            'list-project' => 'list Project for options',
            'view-report' => 'View summary report',
        ]);


    }
}
