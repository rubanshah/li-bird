<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use App\Repositories\VendorRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Project;
use App\ProjectLead;

class ProjectController extends Controller{


    function __construct(VendorRepository $vendorRepo){
        
    }

    function index(){
        return Project::with('leads')->get();
    }

    function find($id){
        return Project::with('leads')->find($id);
    }

    function update($id, Request $request){
        $data = $request->all();

     $validation = Validator::make($data,['name' => 'required'
        ])->validate();

        $project = Project::find($id);
        $project->fill($data);
        $project->save();

        ProjectLead::where('project_id','=',$project->id)->delete();
        $project->leads()->createMany($data['leads']);

        return $project;
    }

    function create(Request $request){
        $data = $request->all();

        $validation = Validator::make($data,['name' => 'required'
        ])->validate();

        $project = new Project();
        $project->fill($data);
        $project->save();

        ProjectLead::where('project_id','=',$project->id)->delete();
        $project->leads()->createMany($data['leads']);

        return $project;
    }

    function delete($id){
        return Project::destroy($id);
    }

}