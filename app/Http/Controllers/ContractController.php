<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use App\Repositories\ContractRepository;
use Illuminate\Http\Request;
use App\ContractAlert;
use App\Contract;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\ContractType;


class ContractController extends Controller{

    private $contractRepo;

    function __construct(ContractRepository $contractRepo){
        $this->contractRepo = $contractRepo;
    }

    function index(Request $request){
        return $this->contractRepo->getAll($request->all());
    }

    function find($id){
        return $this->contractRepo->find($id);
    }

    function update($id, Request $request){
        return $this->contractRepo->update($id, $request->all());
    }

    function create(Request $request){
        return $this->contractRepo->create($request->all());
    }

    function delete($id){
        return $this->contractRepo->delete($id);
    }


    function getAlerts($id){
        return ContractAlert::where('contract_id','=',$id)->get();
    }
  
    function saveAlerts($id, Request $request){
        $d['alerts'] = $request->all();
        $validator = Validator::make($d,['alerts.*.value' => 'required|numeric'])->validate();

        $data = [];
        foreach($d['alerts'] as $alert){
                $data[] = new ContractAlert($alert);
        }

        $contract = Contract::find($id);
        $contract->alerts()->delete();
        return $contract->alerts()->saveMany($data);
    }

    function getLogs($id){
        return $this->contractRepo->getLogs($id);
    }

    function viewLog($cid,$lid){
        return $this->contractRepo->viewLog($cid,$lid);
    }

    function getContractTypes(){
        return ContractType::all();
    }


}