<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Contract;
use App\Notifications\ContractExpire;
use App\Notifications\ContractPaymentDue;
use Notification;

class NotificationController extends Controller{


    function __construct(){
    }

    function index(){
        
    }

    function contractExpiryNotification(){
        $day = env('DEFAULT_NOTIFICATION_TIME', 7); //send notification prior to seven days
        $dt = Carbon::now();
        $dt->addDay($day);

        $date = $dt->toDateString();

        $contracts = Contract::with(['creator','editor','vendor','partialPayment','alerts','contractType','project.leads'])->get();


        foreach ($contracts as $contract) {
            $expiryDates=[];
            $expiryDates[]=$date;

            foreach($contract->alerts as $alert){ //iterate for alerts
                $dt = Carbon::now();
                $dt->addDay($alert->value);
                $date = $dt->toDateString();
                $expiryDates[]=$date;
            }

            //$emails = explode(';', $contract->contractType->email);

            //fetch email from project leads
            $emails = [];
            foreach ($contract->project->leads as $row) {
                    $emails[] = $row->email;
            }
            /////////////////////////////////////


            if(isset($emails[0]) && $emails[0] ){
                    //check if contract is about to expire
                    if(in_array($contract->end_date, $expiryDates)){
                        $notification  = notification::route('mail',$emails[0]);

                        foreach($emails as $email){
                            $notification->route('mail',$email);
                            $notification->notify(new ContractExpire($contract));

                        }
                        //if($contract->vendor)$contract->vendor->notify( new ContractExpire($contract));
                    }

                    //check due date and send dues notification
                    foreach($contract->partialPayment as $payment){
                        if(in_array($payment->due_date,$expiryDates)){
                            $notification  = notification::route('mail',$emails[0]);
                            foreach($emails as $email){
                                $notification->route('mail',$email);
                                $notification->notify(new ContractPaymentDue($contract,$payment));
                            }
                              //  if($contract->vendor)$contract->vendor->notify( new ContractPaymentDue($contract,$payment));
                        }
                    }
            }        

        }//eof contracts foreach



    }

}