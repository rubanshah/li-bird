<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller{

    private $userRepo;

    function __construct(UserRepository $userRepo){
        $this->userRepo = $userRepo;
    }

    function index(){
        return $this->userRepo->getAll();
    }

    function find($id){
        return $this->userRepo->find($id);
    }

    function update($id, Request $request){
        return $this->userRepo->update($id,$request->all());
    }

    function create(Request $request){
        return $this->userRepo->create($request->all());
    }

    function delete($id){
        return $this->userRepo->delete($id);
    }

    function changePassword($id, Request $request){
        return $this->userRepo->changePassword($id, $request->only(['password','password_confirm']) );
    }
}