<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Collection;


class ReportController extends Controller{


    function __construct(){
    }

    function index(){
    }

    function summary(Request $request){
    	$data = $request->all();

        $sql = "SELECT p.name,COUNT(c.id) AS contract_num,SUM(c.amount) AS amount,
                SUM(CASE WHEN `status` = 1 THEN 1 ELSE 0 END) AS status_ongoing, SUM(CASE WHEN `status` = 2 THEN 1 ELSE 0 END) AS status_complete  FROM contracts c JOIN projects p ON(c.project_id=p.id)";

		if((isset($data['start_date']) && $data['start_date']) && (isset($data['end_date']) && $data['end_date'])){
			$sql = $sql .' where c.start_date between ? and ? GROUP BY project_id, p.name';
        	return DB::select($sql,[$data['start_date'],$data['end_date']]);
    	}

    	$sql = $sql.' GROUP BY project_id, p.name';
        
        $result = DB::select($sql);


        if(isset($data['export'])){
            $collection = new Collection();
            $collection->push(['Project','Contracts','Amount','Status(Ongoing/Complete)']);

            foreach($result as $row){
                $d = [$row->name, $row->contract_num, $row->amount, $row->status_ongoing.'/'.$row->status_complete];
                $collection->push($d);
            }
            $filename = 'summary-report.xlsx';
            return $collection->downloadExcel($filename);

        }


        return $result;
    }



}