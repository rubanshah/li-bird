<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Role;
use App\Media;
use Illuminate\Support\Facades\Storage;

class GeneralController extends Controller{


    function __construct(){
    }

    function index(){
    }

    function getRoles(){
        return Role::all();
    }

    function getMedia($filename){
        return response()->file(storage_path("app/contracts/{$filename}"));
    }

	function upload(Request $request){
        $document = $request->file('file');
        $clientOriginalName = $document->getClientOriginalName();
        $clientOriginalExtension = $document->getCLientOriginalExtension();

        $clientOriginalNameWithOutExtensioin = basename($clientOriginalName,'.'.$clientOriginalExtension);
        
        $fileName = $clientOriginalNameWithOutExtensioin.'-'.time().'.'.$clientOriginalExtension;
		$path = $request->file('file')->storeAs('contracts',$fileName);
        
        $fileInfo['name'] = $clientOriginalNameWithOutExtensioin;
        $fileInfo['file_name'] = $fileName;
        $fileInfo['collection_name'] = 'contracts';
        $fileInfo['mime_type'] = $document->getMimeType();

        return Media::create($fileInfo);

	}


}