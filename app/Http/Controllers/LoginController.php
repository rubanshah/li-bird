<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Notifications\ResetPassword;


class LoginController extends Controller{

    function __construct(){

    }


    //remaining add extra logic to validate credentials for verified users
    //add scope for authorization
    function login(Request $request){
        try{
            $jar = new \GuzzleHttp\Cookie\CookieJar();
            $http = new GuzzleHttp\Client;

            $formData = [
                'grant_type' => 'password',
                'client_id' => env('OAUTH_PASSWORD_CLIENT_ID'),
                'client_secret' => env('OAUTH_PASSWORD_CLIENT_SECRET'),
                'username' => $request->input('username'),
                'password' => $request->input('password'),
                'scope'=>''

            ];

            //find user and if exists add scopes
            $user = User::where('email','=',$request->input('username'))->first();
            if($user){
                $permissions = $user->getPermissions();
                $formData['scope'] = implode(' ',$permissions);


                $roles = $user->getRoles();
                if(in_array('administrator',$roles)){
                    $formData['scope'] = '*';
                }
            }
            //////////////////////////////////////

            $response = $http->post(url('oauth/token'),[
                'form_params' => $formData,
                'cookies'=> $jar
            ]);
//            return $response->getBody();
            $response_data = json_decode((string) $response->getBody(), true);

            $user_request = $http->get(url('api/auth/me'),['headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$response_data['access_token']}"
                ]
            ]);

            $response_data['user'] = json_decode((string)$user_request->getBody());

            $user = User::find($response_data['user']->id);

            $response_data['user']->roles = $user->getRoles();
            $response_data['user']->permissions = $user->getPermissions();
            return $response_data;


        } catch(\Exception $e){
            return response($e->getMessage(),$e->getCode());
        }

    }


    public function resetPassword(Request $request){
            try{
                $user = User::where('email','=',$request->input('username'))->firstOrFail();
                $password = str_random(8);//generate random password
                $user->password = Hash::make($password);
                $user->save();

                $user->notify(new ResetPassword($user, $password));
                return response('true');

            }catch(\Exception $e){
                return response($e->getMessage(),404);
            }

    }

    public function logout(Request $request){
        return $request->user()->token()->revoke();
    }

}