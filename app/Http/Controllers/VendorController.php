<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use App\Repositories\VendorRepository;
use Illuminate\Http\Request;

class VendorController extends Controller{

    private $vendorRepo;

    function __construct(VendorRepository $vendorRepo){
        $this->vendorRepo = $vendorRepo;
    }

    function index(){
        return $this->vendorRepo->getAll();
    }

    function find($id){
        return $this->vendorRepo->find($id);
    }

    function update($id, Request $request){
        return $this->vendorRepo->update($id,$request->all());
    }

    function create(Request $request){
        return $this->vendorRepo->create($request->all());
    }

    function delete($id){
        return $this->vendorRepo->delete($id);
    }

}