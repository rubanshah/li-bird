<?php
/**
 * Created by PhpStorm.
 * User: ruban
 * Date: 4/19/2017
 * Time: 9:09 AM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Role;
use App\Media;
use Illuminate\Support\Facades\Storage;
use App\ContractType;

class SettingsController extends Controller{


    function __construct(){
    }

    function index(){
    }

    function saveContractEmail(Request $request){
        $data = $request->all();
        foreach ($data as $row) {
            $contractType = ContractType::find($row['id']);
            $contractType->fill($row);
            $contractType->save();
        }
        return ContractType::all();
    }

}